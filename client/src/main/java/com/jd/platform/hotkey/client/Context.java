package com.jd.platform.hotkey.client;

/**
 * 相当于一个配置类了
 *
 * @author wuweifeng wrote on 2019-12-05
 * @version 1.0
 */
public class Context {
    /**
     * 应用名称
     */
    public static String APP_NAME;

    /**
     * 与worker连接断开后，是否需要重连
     */
    public static boolean NEED_RECONNECT = true;

    /**
     * caffeine的最大容量
     */
    public static int CAFFEINE_SIZE;
}
